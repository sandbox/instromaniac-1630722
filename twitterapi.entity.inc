<?php

/**
 * @file
 * Entity related CRUD functions. 
 */

/**
 * Load a tweet.
 * 
 * @param int $twitter_id 
 *   The numerical twitter id of the tweet.
 * @return TwitterAPITweet
 *   The corresponding tweet entity or FALSE if it doesn't exist.
 */
function twitterapi_tweet_load($twitter_id) {
  $result = entity_load('twitterapi_tweet', array($twitter_id));
  return $result ? reset($result) : FALSE;
}

/**
 * Load multiple tweets at once.
 * 
 * @param array $twitter_ids
 *   An array of twitter ids.
 * @param array $conditions 
 *   An array of conditions to be met for each tweet.
 */
function twitterapi_tweet_load_multiple($twitter_ids, $conditions = array()) {
  return entity_load('twitterapi_tweet', $twitter_ids, $conditions);
}

/**
 * Save a tweet.
 * 
 * @param TwitterAPITweet $tweet
 *   The tweet entity to be saved.
 * 
 * @see entity_save
 */
function twitterapi_tweet_save($tweet) {
  return entity_save('twitterapi_tweet', $tweet);
}

/**
 * Delete multiple tweet entities.
 * 
 * @param array $twitter_ids 
 *   An array of twitter ids to delete.
 */
function twitterapi_tweet_delete_multiple($twitter_ids = array()) {
  entity_delete_multiple('twitterapi_tweet', $twitter_ids);
}

/**
 * Access callback for the twitterapi_tweet entity.
 */
function twitterapi_tweet_access($op, $entity, $account, $entity_type) {
  return user_access('create twitterapi tweets', $account);
}

/**
 * Load a twitter user.
 * 
 * @param int $twitter_id 
 *   The numerical twitter id of the user.
 * @return TwitterAPIUser
 *   The corresponding twitter user entity or FALSE if it doesn't exist.
 */
function twitterapi_twitter_user_load($twitter_id) {
  $result = entity_load('twitterapi_user', array($twitter_id));
  return $result ? reset($result) : FALSE;
}

/**
 * Load multiple twitter users at once.
 * 
 * @param array $twitter_ids
 *   An array of twitter ids.
 * @param array $conditions 
 *   An array of conditions to be met for each twitter user.
 */
function twitterapi_twitter_user_load_multiple($twitter_ids, $conditions = array()) {
  return entity_load('twitterapi_user', $twitter_ids, $conditions);
}

/**
 * Save a twitter user.
 * 
 * @param TwitterAPIUser $twitter_user
 *   The twitterapi_user entity to be saved.
 * 
 * @see entity_save
 */
function twitterapi_twitter_user_save($twitter_user) {
  return entity_save('twitterapi_user', $twitter_user);
}

/**
 * Delete multiple twitter user entities.
 * 
 * @param array $twitter_ids 
 *   An array of twitter ids to delete.
 */
function twitterapi_twitter_user_delete_multiple($twitter_ids = array()) {
  entity_delete_multiple('twitterapi_user', $twitter_ids);
}

/**
 * Access callback for the twitterapi_tweet entity.
 */
function twitterapi_twitter_user_access($op, $entity, $account, $entity_type) {
  return user_access('create twitterapi users', $account);
}
