<?php

/**
 * @file
 * Provides Entity metadata integration.
 */

/**
 * MetadataController for twitterapi_tweet entities.
 */
class TwitterAPITweetMetadataController extends EntityDefaultMetadataController {

  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();
    $properties = &$info[$this->type]['properties'];

    $properties['user'] = array(
      'label' => t("User"),
      'type' => 'user',
      'description' => t("The user that authored this tweet."),
      'getter callback' => 'entity_property_getter_method',
      'setter callback' => 'entity_property_setter_method',
      'schema field' => 'user_id',
      'required' => FALSE,
    );
    
    $properties['twitter_user'] = array(
      'label' => t("Twitter User"),
      'type' => 'twitterapi_user',
      'description' => t("The Twitter account that authored this tweet."),
      'getter callback' => 'entity_property_getter_method',
      'setter callback' => 'entity_property_setter_method',
      'schema field' => 'user_id',
      'required' => TRUE,
    );

    $properties['changed_time'] = array(
      'type' => 'date',
      'setter callback' => 'entity_property_verbatim_set',
      'description' => t('The time the tweet was last changed.'),
    ) + $properties['changed_time'];
    
    $properties['created_time'] = array(
      'type' => 'date',
      'setter callback' => 'entity_property_verbatim_set',
      'description' => t('The time the tweet was created.'),
    ) + $properties['created_time'];

    return $info;
  }
}

/**
 * MetadataController for twitterapi_user entities.
 */
class TwitterAPIUserMetadataController extends EntityDefaultMetadataController {

  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();
    $properties = &$info[$this->type]['properties'];

    $properties['user'] = array(
      'label' => t("User"),
      'type' => 'user',
      'description' => t("The user that authored this tweet."),
      'getter callback' => 'entity_property_getter_method',
      'setter callback' => 'entity_property_setter_method',
      'schema field' => 'user_id',
      'required' => FALSE,
    );

    $properties['changed_time'] = array(
      'type' => 'date',
      'setter callback' => 'entity_property_verbatim_set',
      'description' => t('The time the tweet was last changed.'),
    ) + $properties['changed_time'];
    
    $properties['created_time'] = array(
      'type' => 'date',
      'setter callback' => 'entity_property_verbatim_set',
      'description' => t('The time the tweet was created.'),
    ) + $properties['created_time'];

    return $info;
  }
}
