<?php

/**
 * A Twitter User Entity class. 
 */
class TwitterAPIUser extends Entity {
  
  public function __construct($values = array()) {
    parent::_construct($values);
  }
  
  /**
   * Returns the user associated with this Twitter user if one exists.
   * 
   * @return mixed
   *   Either a drupal user object or NULL if no drupal user is associated with
   *   this tweet. 
   */
  public function getUser() {
  }
  
  /**
   * Implements Entity::save().
   * 
   * Adds some additional meta data to the tweet before saving it. 
   */
  public function save() {
    if (!empty($this->is_new)) {
      $this->created = REQUEST_TIME;
      if (isset($this->created_at) && ($created_time = strtotime($this->created_at))) {
        $this->created_time = $created_time;
      }
    }
    $this->changed_time = REQUEST_TIME;
    parent::save();
  }
}
