The Twitter API module is used for supporting other modules that want to 
integrate with Twitter. It does not provide any functionality on its own, other
than providing integration with Twitter and data storage.

The following integration is implemented:

1. Twitter REST API library
2. Twitter status entity CRUD
3. Twitter account entity CRUD

Planned:

1. Twitter Streaming API library

TODO:
- test entities
- add support for libraries
- add rest lib
- add oath support